# Web server(s) hosting the input data for this pipeline.
#
# This is the web page containing the files that must be located in the
# `SURVEY' directory of `reproduce/config/pipeline/LOCAL.mk' on the local
# system.
web-survey = https://some.webpage.com/example/server
