\documentclass[10pt, twocolumn]{article}

%% This is a convenience variable if you are using PGFPlots to build plots
%% within LaTeX. If you want to import PDF files for figures directly, you
%% can use the standard `\includegraphics' command. See the definition of
%% `\includetikz' in `tex/preamble-pgfplots.tex' for where the files are
%% assumed to be if you use `\includetikz' when `\makepdf' is not defined.
\newcommand{\makepdf}{}

%% When defined (value is irrelevant), `\highlightchanges' will cause text
%% in `\tonote' and `\new' to become colored. This is useful in cases that
%% you need to distribute drafts that is undergoing revision and you want
%% to hightlight to your colleagues which parts are new and which parts are
%% only for discussion.
\newcommand{\highlightchanges}{}

%% Necessary LaTeX preambles to include for relevant functionality. We want
%% to start this file as fast as possible with the actual body of the
%% paper, while keeping modularity in the preambles.
\input{tex/pipeline.tex}
\input{tex/preamble-style.tex}
\input{tex/preamble-header.tex}
\input{tex/preamble-biblatex.tex}
\input{tex/preamble-pgfplots.tex}
\input{tex/preamble-necessary.tex}










%% Start writing.
\begin{document}

%% Abstract, keywords and reproduction pipeline notice.
\includeabstract{

  You have completed the reproduction pipeline and are ready to configure
  and implement it for your own research. This template reproduction
  pipeline and document contains almost all the elements that you will need
  in a research project containing the downloading of raw data, processing
  it, including them in plots and report, including this abstract, figures
  and bibliography. If you use this pipeline in your work, don't forget to
  add a notice to clearly let the readers know that your work is
  reproducible. If this pipeline proves useful in your research, please
  cite \citet{ai15}.

  \vspace{0.25cm}

  \textsl{Keywords}: Add some keywords for your research here.

  \textsl{Reproducible paper}: Generated from reproduction pipeline
  \pipelineversion{} and Gnuastro \gnuastroversion. Pipeline available at:
  \url{https://link-to.the/git/repo-of-your-pipeline}.
}

%% To add the first page's headers.
\thispagestyle{firststyle}





%% Start of the main body of text.
\section{Congratulations!}
Congratulations on running the reproduction pipeline! You can now follow
the checklist in the \texttt{README.md} file to customize this pipeline to
your exciting research project.

Just don't forget to \emph{never} use any numbers or fixed strings (for
example database urls like \url{\websurvey}) directly within your \LaTeX{}
source. Read them directly from your configuration files or outputs of the
programs as part of the reproduction pipeline and import them into \LaTeX{}
as macros through the \texttt{tex/pipeline.tex} file. See the several
examples within the pipeline for a demonstration. For some recent
real-world examples, the reproduction pipelines for Sections 4 and 7.3 of
\citet{bacon17} are available at
\href{https://doi.org/10.5281/zenodo.1164774}{zenodo.1164774}\footnote{\url{https://gitlab.com/makhlaghi/muse-udf-origin-only-hst-magnitudes}},
or
\href{https://doi.org/10.5281/zenodo.1163746}{zenodo.1163746}\footnote{\url{https://gitlab.com/makhlaghi/muse-udf-photometry-astrometry}}. Working
in this way, will let you focus clearly on your science and not have to
worry about fixing this or that number/name in the text.

Just as a demonstration of creating plots within \LaTeX{} (using the
{\small PGFP}lots package), in Figure \ref{deleteme} we show a simple
plot, where the Y axis is the square of the X axis. The minimum value
in this distribution is $\deletememin$, and $\deletememax$ is the
maximum. Take a look into the \LaTeX{} source and you'll see these
numbers are actually macros that were calculated from the same dataset
(they will change if the dataset, or function that produced it,
changes).

The {\small PDF} file of Figure \ref{deleteme} is available in the
directory \texttt{\bdir/tex/build/tikz/} and can be used in other contexts
(for example slides). If you want to directly use the {\small PDF} file in
the figure without having to let {\small T}i{\small KZ} decide if it should
be remade or not, you can also comment the \texttt{makepdf} macro at the
top of this \LaTeX{} source file.

{\small PGFP}lots is a great tool to build the plots within \LaTeX{} and
removes the necessity to add further dependencies (to create the plots) to
your reproduction pipeline. High-level language libraries like Matplotlib
do exist to also generate plots. However, bare in mind that they require
many dependencies (Python, Numpy and etc). Installing these dependencies
from source (after several years when the binaries are no longer available
in common repositories), is not easy and will harm the reproducibility of
your paper.

\begin{figure}[t]
  \includetikz{delete-me}

  \captionof{figure}{\label{deleteme} A very basic $X^2$ plot for
    demonstration.}
\end{figure}

Furthermore, since {\small PGFP}lots is built by \LaTeX{} it respects all
the properties of your text (for example line width and fonts and etc), so
the final plot blends in your paper much more nicely. It also has a
wonderful
manual\footnote{\url{http://mirrors.ctan.org/graphics/pgf/contrib/pgfplots/doc/pgfplots.pdf}}.

This pipeline also defines two \LaTeX{} macros that allow you to mark text
within your document as \emph{new} and \emph{notes}. For example, \new{this
  text has been marked as \texttt{new}.} \tonote{While this one is marked
  as \texttt{tonote}.} If you comment the line (by adding a `\texttt{\%}'
at the start of the line or simply deleting the line) that defines
\texttt{highlightchanges}, then the one that was marked \texttt{new} will
become black (totally blend in with the rest of the text) and the one
marked \texttt{tonote} will not be in the final PDF. You can thus use
\texttt{highlightchanges} to easily make copies of your research for
existing coauthors (who are just interested in the new parts or notes) and
new co-authors (who don't want to be distracted by these issues in their
first time reading).





\section{Notice and citations}
To encourage other scientists to publish similarly reproducible papers,
please add a notice close to the start of your paper or in the end of the
abstract clearly mentioning that your work is fully reproducible.

For the time being, we haven't written a specific paper only for this
reproduction pipeline, so until then, we would be grateful if you could
cite the first paper that used the first version of this pipeline:
\citet{ai15}.

After publication, don't forget to upload all the necessary data, software
source code and the reproduction pipeline to a long-lasting host like
Zenodo (\url{https://zenodo.org}).





%% Tell BibLaTeX to put the bibliography list here.
\printbibliography

%% Finish LaTeX
\end{document}
